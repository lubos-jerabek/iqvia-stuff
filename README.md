# iqvia-stuff

**Complete Behave tests**  

-   All the tests are green now
-   I would probably try to unify the way used to find elements. Ideally with some data attributes
-   Also there wasn't any test for both invalid username and password at the same time
-   From test automation point of view I'd add probably try to develop a framework around usual operations:

-   Page object model - way how to find certain elements in a DOM
-   Browser abilities - basically having a bunch of methods around browser - and standardise test automation methods

-   However - I haven't had a chance to use Behave before and it was a very pleasant experience! I would absolutely consider it for some of my future projects.


  
**Postman**

-   I didn't understand why did the API return 200 when it should return 4xx
-   This was my first experience with Postman automation - not sure if it's supposed to be like this but:

-   I haven't found a way how to use variables in the expected result part
-   The code itself is not very well readable
-   There seems to be a lot of repeated code - basically those calls are different in just a few things. It would be nice to extract that into a method - not sure if it's possible with postman?  
    
-   Every API response should get status code checked - added assertion to every call
-   Username, password and path should be parametrised - added that too
-   Again, I missed some tests so I added a few (incorrect login because of both invalid username and password.)

