from behave import *
from login_page import (
    username_field,
    password_field,
    login_button
)
from browser import elements_containing

BASE_URL = 'https://sprinkle-burn.glitch.me/'
USERNAME = 'test@drugdev.com'
PASSWORD = 'supers3cret'
WELCOME_MSG = 'Welcome Dr I Test'
ERROR_MSG = 'Credentials are incorrect'


@given(u'the user has the correct credentials')
def step_impl(context):
    context.browser.get(BASE_URL)
    context.username = USERNAME
    context.password = PASSWORD


@when(u'the user enters username')
def step_impl(context):
    username_field(context).send_keys(context.username)


@when(u'the user enters password')
def step_impl(context):
    password_field(context).send_keys(context.password)


@when(u'clicks Login')
def step_impl(context):
    login_button(context).click()


@then(u'the user is presented with a welcome message')
def step_impl(context):
    assert elements_containing(context, WELCOME_MSG)


@given(u'the user has the incorrect username')
def step_impl(context):
    context.browser.get(BASE_URL)
    context.username = USERNAME[::-1]
    context.password = PASSWORD


@given(u'the user has the incorrect password')
def step_impl(context):
    context.browser.get(BASE_URL)
    context.username = USERNAME
    context.password = PASSWORD[::-1]


@then(u'the user is presented with a error message')
def step_impl(context):
    assert elements_containing(context, ERROR_MSG)
