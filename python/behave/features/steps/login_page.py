from browser import element_by_name, element_by_aria

def username_field(context):
    return element_by_name(context, 'email')


def password_field(context):
    return element_by_name(context, 'password')


def login_button(context):
    return element_by_aria(context, 'login')
