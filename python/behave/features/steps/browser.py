def aria(selector):
    return '[aria-label="{}"]'.format(selector)


def contains(text):
    return '//*[contains(., "{}")]'.format(text)


def element_by_aria(context, selector):
    return context.browser.find_element_by_css_selector(aria(selector))


def elements_containing(context, text):
    return context.browser.find_element_by_xpath(contains(text))


def element_by_name(context, selector):
    return context.browser.find_element_by_name(selector)


def element_by_tag(context, selector):
    return context.browser.find_element_by_tag_name(selector)
