from selenium import webdriver

DEFAULT_TIMEOUT = 5

def before_all(context):
    context.browser = webdriver.Chrome()
    context.browser.implicitly_wait(DEFAULT_TIMEOUT)
    context.username = ''
    context.password = ''

def after_all(context):
	context.browser.quit()
